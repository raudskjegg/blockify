"""
This script mutes the commercials in Spotify automatically in Windows 7.
It has been tested with Python 2.6.5 (32 bit). You'll need the
Win32all (pywin32) add-on extension, which can be downloaded
from  http://sourceforge.net/projects/pywin32/
You will also need to put the volumeContr.dll (EndpointVolume API wrapper)
in the same directory as this script.

Just run this script alongside Spotify (just double-click it),
and block a commercial by pressing
Ctrl+B  when in the console window.
The commercial should then be added to the list of blocked commercials and muted.
"""

import os, sys, time, msvcrt, ctypes, win32gui, win32con

debug = False

def dprint(item):
    if debug:
        print item

## Load the volume control dll to access the windows IAudioEndpointDevice API
volumeLib = ctypes.cdll.LoadLibrary(os.path.abspath("volumeContr.dll"))

## Set expected argument and return types
volumeLib.SetMute.argtypes = [ctypes.c_bool]
volumeLib.GetMute.restype = ctypes.c_bool

## Path to the file containing the blocked patterns
if getattr(sys, 'frozen', None):
    basedir = sys._MEIPASS
else:
    basedir = os.path.dirname(sys.argv[0])

dprint(basedir)
    
PATTERNFILE = os.path.join(os.path.abspath(basedir), "blockPatterns.txt")

newline = os.linesep

def GetBlockPatternList():
    """Get the list of blocked patterns and put them in a list"""
    
    patternFile = file(PATTERNFILE, "rb")
    patternList = []

    for pattern in patternFile:

        if (pattern == newline): continue

        patternList.append(pattern)
        
    patternFile.close()
    
    return patternList

def AddWindowTitleToPatternList(hwnd):
    """Add window title pattern to list and file.

    Takes window handle as argument.
    """

    global patternList

    windowTitle = win32gui.GetWindowText(hwnd) + newline
    
    patternList.append(windowTitle)

    AddPatternToFile(windowTitle)

    return

def AddPatternToFile(newPattern):
    """Takes string as argument."""
    
    patternFile = file(PATTERNFILE, "a+")
    patternList = []
    
    for existingPattern in patternFile:
        patternList.append(existingPattern)
        
    if (patternList.count(newPattern) == 0):
        patternFile.write(newPattern)
        
    patternFile.close()

    return

def windowEnumerationHandler(hwnd, resultList):
    """Pass to win32gui.EnumWindows() to generate list of [window handle, window text] tuples."""
    
    resultList.append((hwnd, win32gui.GetWindowText(hwnd)))
    return

def findTopWindow(wantedText=None):
    """Find the top window that starts with the string wantedText."""
    
    topWindows = []

    win32gui.EnumWindows(windowEnumerationHandler, topWindows)

    for hwnd, windowText in topWindows:

        if wantedText and not windowText.startswith(wantedText):
            continue
        
        return hwnd
    
    return None

def MuteOn():
    """Return True if mute was set (not already muted), else return False."""
    if volumeLib.GetMute() == 0:
        volumeLib.SetMute(1)
        return True
    return False

def MuteOff():
    """Return True if mute was reset (not already unmuted), else return False."""
    if volumeLib.GetMute() == 1:
        volumeLib.SetMute(0)
        return True
    return False

def sendKeystroke(hwnd, key=None):
    """Send keystroke to window identified by hwnd.

    Tip: Space key is win32con.VK_SPACE
    """
    win32gui.SendMessage(hwnd, win32con.WM_KEYDOWN, key, 0)
    win32gui.SendMessage(hwnd, win32con.WM_KEYUP, key, 0)


if (not os.path.exists(PATTERNFILE)):
    open(PATTERNFILE, 'w').close()

MuteOff()
patternList = GetBlockPatternList()
space_key = win32con.VK_SPACE

print "##########                     BLOCKIFY                     ##########"
print "Press Ctrl+B to block a commercial, Ctrl+L to list blocked commercials\n"
print "Got pattern list:\n"
print ''.join(patternList)

while True:

    found = False

    hspfy = findTopWindow("Spotify -")

    if hspfy is not None:
        spfy_pattern = win32gui.GetWindowText(hspfy)
        for pattern in patternList:
            p = pattern.strip(newline)
            if p == spfy_pattern or spfy_pattern.startswith("Spotify - Spotify"):
                found = True
                break

    if found:
        if MuteOn() :
            print "Pattern found: ", pattern
            print "Mute on" + newline

            # We are muted, now press play in Spotify to finish the commercial
            time.sleep(1)
            sendKeystroke(hspfy, space_key)

            print "Keystroke sent" + newline
    else:
        if MuteOff():
            print "Mute off" + newline


    ### This section is for using a keyboard shortcut to add commercials to blocked list.
            
    ## Poll keyboard (non-blocking), when keystroke is pressed get the key(s).
    ## To test which keys or key-combos which are supported run this little script:
    
    ##    # Key test
    ##    while True:
    ##        if msvcrt.kbhit():
    ##            key = msvcrt.getch()
    ##            print repr(key)+',',
            
    ## Ctrl+B = '\x02', Ctrl+L = '\0xc', Ctrl+Q = '\x11'
            
    if msvcrt.kbhit():

        if msvcrt.getch() == '\x02':
            
            if spfy_pattern is not "Spotify" and spfy_pattern is not newline:
                
                #To weed out the times when the title is just 'Spotify'
                #or just whitespace (in between songs?)
                AddWindowTitleToPatternList(hspfy)
                print "Pattern added to blocked pattern list:\n\t", spfy_pattern, newline

        elif msvcrt.getch() == '\x0c':
            
            print ''.join(patternList)

        # Clear the keyboard buffer
        while msvcrt.kbhit(): msvcrt.getch()

    time.sleep(0.1)

