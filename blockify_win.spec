# -*- mode: python -*-
a = Analysis(['blockify_win.py'],
             pathex=['C:\\Users\\Andre\\repos\\blockify'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\blockify_win', 'blockify.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               [('volumeContr.dll','C:\\Users\\Andre\\repos\\blockify\\volumeContr.dll','BINARY'),
		('msvcr100d.dll','C:\\Windows\\system','BINARY'),
                ('blockPatterns.txt', 'C:\\Users\\Andre\\repos\\blockify\\blockPatterns.txt','DATA')],
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name=os.path.join('dist', 'blockify_win'))
